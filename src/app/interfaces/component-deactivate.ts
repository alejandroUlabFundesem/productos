export interface ComponentDeactivate {
    canDeactivate() : Promise<boolean> | boolean ;
}