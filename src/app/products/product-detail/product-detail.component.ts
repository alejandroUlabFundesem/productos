import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IProduct } from '../interfaces/i-product';
import { ProductsService } from '../services/products.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  product: IProduct;
  
  constructor(
    private route: ActivatedRoute, 
    private productsService: ProductsService, 
    private router: Router) { }

  ngOnInit(): void {
    this.product = this.route.snapshot.data['product'];
  }

  goBack() { 
    this.router.navigate(['/products']); 
  }

  edit() { 
    this.router.navigate(['/products/edit', this.product.id]); 
  }

}
