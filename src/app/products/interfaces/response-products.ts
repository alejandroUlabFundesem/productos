import { IProduct } from './i-product';

export interface ResponseProducts {
    products: IProduct[];
}

export interface HttpErrorResponse {
    status: number;
    message: string;
}

export interface OkResponse {
    ok: boolean;
    error?: string;
}

export interface ProductResponse { 
    ok: boolean; 
    product: IProduct; 
    error?: string; 
}