import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PRODUCTS_ROUTES } from './products.routes';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductItemComponent } from './product-item/product-item.component';
import { ProductFilterPipe } from './pipes/product-filter.pipe';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ProductsService } from './services/products.service';
import { ProductDetailGuard } from './guards/product-detail.guard';
import { ProductDetailResolveGuard } from './guards/product-detail-resolve.guard';


@NgModule({
  declarations: [
    ProductListComponent,
    ProductItemComponent,
    ProductFilterPipe,
    ProductDetailComponent,
    ProductEditComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forChild(PRODUCTS_ROUTES)
  ], 
  providers: [
    ProductsService,
    ProductDetailGuard,
    ProductDetailResolveGuard
  ]
})
export class ProductsModule { }
