import { Component, OnInit } from '@angular/core';
import { ComponentDeactivate } from '../../interfaces/component-deactivate';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit, ComponentDeactivate {

  constructor() { }

  ngOnInit(): void {
  }

  canDeactivate() : Promise<boolean> { 
    return Swal.fire({
      title: '¿Seguro que quieres abandonar la página?',
      text: 'Los cambios no se guardarán',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, vámonos',
      cancelButtonText: 'No, nos quedamos'
    })
    .then(result => result.value);
  }
}
