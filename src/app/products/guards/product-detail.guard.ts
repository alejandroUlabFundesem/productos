import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductDetailGuard implements CanActivate {
  constructor(private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const id = +next.params['id']; 
      
      if (isNaN(id) || id < 1) { // La id no es numérica o no es válida 
        alert('Invalid product id!'); 
        this.router.navigate(['/products']); // Vamos al listado de productos 
        return false; // No se puede activar la ruta 
      } 
      
      return true;
  }
  
}
