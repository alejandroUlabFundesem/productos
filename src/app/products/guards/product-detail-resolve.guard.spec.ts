import { TestBed } from '@angular/core/testing';

import { ProductDetailResolveGuard } from './product-detail-resolve.guard';

describe('ProductDetailResolveGuard', () => {
  let guard: ProductDetailResolveGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ProductDetailResolveGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
