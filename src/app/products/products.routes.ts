import { Route } from '@angular/router'; 
import { ProductListComponent } from './product-list/product-list.component'; 
import { ProductDetailComponent } from './product-detail/product-detail.component'; 
import { ProductDetailGuard } from './guards/product-detail.guard';
import { LeavePageGuard } from '../guards/leave-page.guard';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductDetailResolveGuard } from './guards/product-detail-resolve.guard';

export const PRODUCTS_ROUTES: Route[] = [
    { path: 'products', component: ProductListComponent }, // :id es un parámetro (id del producto) 
    { 
        path: 'products/:id', 
        canActivate: [ ProductDetailGuard ],
        component: ProductDetailComponent, 
        resolve: { 
            product: ProductDetailResolveGuard 
        } 
    }, // Ruta por defecto (vacía) -> Redirigir a /welcome 
    { 
        path: 'products/edit/:id', 
        canActivate: [ProductDetailGuard],
        canDeactivate: [ LeavePageGuard ], 
        component: ProductEditComponent 
    },
];